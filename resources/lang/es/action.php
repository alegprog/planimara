<?php
return [
  'create'=>'Crear',
  'edit'=>'Editar',
  'save'=>'Guardar',
  'delete'=>'Eliminar',
  'update'=>'Modificar',
  'show'=>'Ver',
  'register'=>'Registrame',
  'login'=>'Iniciar sesión',
  'password_reset'=>'¿Olvidaste tu contraseña?',
  'remember'=>'Recordar mis datos',
  'reset_password'=>'Restablecer la contraseña',
  'send_password'=>'Enviar contraseña',
];
