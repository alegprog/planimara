@extends('template.page')

@section('css')
  <link href="{{url('/')}}/css/estilos.css" rel="stylesheet" type="text/css">
  
@endsection

@section('titulo')
  Inicio
@endsection

@section('content')
<section class="articles">
<article>
    <table width="116%">
      <tr>
        <th height="373">
          <article class="post clearfix">


                <div class="zoom">
                  <div align="left"><a href="#" class="thumb pull-left"><img class="img-thumbnail" src="imagenes/investigacion.jpg " title="Investigación a la medida de pequeños agricultores" alt="400" width="400">             </a></div>
                </div>
            <h2 align="left" class="post-title">
              <a href="#"> Investigación a la medida de pequeños agricultores</a></h2>
            <p align="left"><span class="post-fecha">24 de Enero 2017</span> por <span class="post-autor"><a href="#">Wanyini núñez</a>   </span> </p>
            <p align="justify" class="post-contenido text-justify">En los sectores rurales de Venezuela serán sembradas 2,4 de hectáreas, a fin de producir 20 millones de alimentos sanos y de calidad a corto y mediano plazo.En los sectores rurales de Venezuela serán sembradas 2,4 de hectáreas, a fin de producir 20 millones de alimentos sanos y de calidad a corto y mediano plazo.</p>
            <div class="contenedor-botones">
              <div align="left"><a href="#" class="btn btn-success">Leer artículo</a> </div>
            </div>
        </article>          </th>
          <th >
            <article class="post clearfix">

                <div class="zoom">
                  <div align="left"><a href="#" class="thumb pull-left"><img class="img-thumbnail" src="imagenes/agricultura.jpg" title="La renta de agricultores y ganaderos cae a niveles de 2000" alt="300" width="300">          </a></div>
                </div>
                <h2 align="left" class="post-title">
                <a href="#"> La renta de agricultores y ganaderos cae a niveles de 2000</a></h2>
            <p align="left"><span class="post-fecha">24 de Enero 2017</span> por <span class="post-autor"><a href="#">Wanyini núñez</a>   </span> </p>
            <p align="left" class="post-contenido text-justify"> En los sectores rurales de Venezuela serán sembradas 2,4 de hectáreas, a fin de producir 20 millones de alimentos sanos y de calidad a corto y mediano plazo.En los sectores rurales de Venezuela serán sembradas 2,4 de hectáreas, a fin de producir 20 millones de alimentos sanos y de calidad a corto y mediano plazo.</p>
            <div class="contenedor-botones">
              <div align="left"><a href="#" class="btn btn-success">Leer artículo</a> </div>
            </div>
            </article></th>
        </tr>
    </table>
     <table width="116%">
      <tr>
        <th height="373">
          <article class="post clearfix">


                <div class="zoom">
                  <div align="left"><a href="#" class="thumb pull-left"><img class="img-thumbnail" src="imagenes/renta.jpg" title="Investigación a la medida de pequeños agricultores" alt="400" width="400">             </a></div>
                </div>
            <h2 align="left" class="post-title">
              <a href="#"> Investigación a la medida de pequeños agricultores</a></h2>
            <p align="left"><span class="post-fecha">24 de Enero 2017</span> por <span class="post-autor"><a href="#">Wanyini núñez</a>   </span> </p>
            <p align="justify" class="post-contenido text-justify">En los sectores rurales de Venezuela serán sembradas 2,4 de hectáreas, a fin de producir 20 millones de alimentos sanos y de calidad a corto y mediano plazo.En los sectores rurales de Venezuela serán sembradas 2,4 de hectáreas, a fin de producir 20 millones de alimentos sanos y de calidad a corto y mediano plazo.</p>
            <div class="contenedor-botones">
              <div align="left"><a href="#" class="btn btn-success">Leer artículo</a> </div>
            </div>
        </article>          </th>
          <th >
            <article class="post clearfix">

                <div class="zoom">
                  <div align="left"><a href="#" class="thumb pull-left"><img class="img-thumbnail" src="imagenes/investigacion.jpg" title="La renta de agricultores y ganaderos cae a niveles de 2000" alt="300" width="300">          </a></div>
                </div>
                <h2 align="left" class="post-title">
                <a href="#"> La renta de agricultores y ganaderos cae a niveles de 2000</a></h2>
            <p align="left"><span class="post-fecha">24 de Enero 2017</span> por <span class="post-autor"><a href="#">Wanyini núñez</a>   </span> </p>
            <p align="left" class="post-contenido text-justify"> En los sectores rurales de Venezuela serán sembradas 2,4 de hectáreas, a fin de producir 20 millones de alimentos sanos y de calidad a corto y mediano plazo.En los sectores rurales de Venezuela serán sembradas 2,4 de hectáreas, a fin de producir 20 millones de alimentos sanos y de calidad a corto y mediano plazo.</p>
            <div class="contenedor-botones">
              <div align="left"><a href="#" class="btn btn-success">Leer artículo</a> </div>
            </div>
            </article></th>
        </tr>
    </table>
     <table width="116%">
      <tr>
        <th height="373">
          <article class="post clearfix">


                <div class="zoom">
                  <div align="left"><a href="#" class="thumb pull-left"><img class="img-thumbnail" src="imagenes/investigacion.jpg " title="Investigación a la medida de pequeños agricultores" alt="400" width="400">             </a></div>
                </div>
            <h2 align="left" class="post-title">
              <a href="#"> Investigación a la medida de pequeños agricultores</a></h2>
            <p align="left"><span class="post-fecha">24 de Enero 2017</span> por <span class="post-autor"><a href="#">Wanyini núñez</a>   </span> </p>
            <p align="justify" class="post-contenido text-justify">En los sectores rurales de Venezuela serán sembradas 2,4 de hectáreas, a fin de producir 20 millones de alimentos sanos y de calidad a corto y mediano plazo.En los sectores rurales de Venezuela serán sembradas 2,4 de hectáreas, a fin de producir 20 millones de alimentos sanos y de calidad a corto y mediano plazo.</p>
            <div class="contenedor-botones">
              <div align="left"><a href="#" class="btn btn-success">Leer artículo</a> </div>
            </div>
        </article>          </th>
          <th >
            <article class="post clearfix">

                <div class="zoom">
                  <div align="left"><a href="#" class="thumb pull-left"><img class="img-thumbnail" src="imagenes/agricultura.jpg" title="La renta de agricultores y ganaderos cae a niveles de 2000" alt="300" width="300">          </a></div>
                </div>
                <h2 align="left" class="post-title">
                <a href="#"> La renta de agricultores y ganaderos cae a niveles de 2000</a></h2>
            <p align="left"><span class="post-fecha">24 de Enero 2017</span> por <span class="post-autor"><a href="#">Wanyini núñez</a>   </span> </p>
            <p align="left" class="post-contenido text-justify"> En los sectores rurales de Venezuela serán sembradas 2,4 de hectáreas, a fin de producir 20 millones de alimentos sanos y de calidad a corto y mediano plazo.En los sectores rurales de Venezuela serán sembradas 2,4 de hectáreas, a fin de producir 20 millones de alimentos sanos y de calidad a corto y mediano plazo.</p>
            <div class="contenedor-botones">
              <div align="left"><a href="#" class="btn btn-success">Leer artículo</a> </div>
            </div>
            </article></th>
        </tr>
    </table>

</article>
</section>

@endsection
