@extends('template.page')

@section('css')
  <link href="{{url('/')}}/css/estilos/cedulaestilos.css" rel="stylesheet">
  <style>
  input[type="text"],input[type="password"] {

    background-color: #dcdcdc;
    width: 100%;

  }
  .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6,
  .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {

    background-color: transparent !important;
}
  
  .slider {
    margin-bottom: 0px;
    padding-bottom: 0px;
  }
  .slider ul{
    margin-bottom: 0px;
    padding-bottom: 0px;
  }
  .slider li{
    margin-bottom: 0px;
    padding-bottom: 0px;
  }
  .slider img{
    margin-bottom: 0px;
    padding-bottom: 0px;
  }
  </style>
@endsection

@section('titulo')
  Inicio de Sesión
@endsection

@section('content')
  <section class="articles">
    <article>
    @if(isset($error))
        <div class="alert alert-info">
          <b>Ofrecemos disculpas,<br></b>
          {{ session('status_error') }}
        </div>
    @endif

    @if(session('status_error'))
        <div class="alert alert-danger">
          <b>Ofrecemos disculpas,<br></b>
          {{ session('status_error') }}
        </div>
    @endif

    @if(session('status_success'))
        <div class="alert alert-success">
          <b>Excelente!<br></b>
          {{ session('status_success') }}
        </div>
    @endif
    <br>
    <h2 class="text-center"><strong>Inicio de Sesión</strong> </h2>
    <br>

        <br>

 <form action="{{url('/login')}}" class="form-horizontal" method="post">
  {{ csrf_field() }}

  <div class="form-group">
    <label for="username" class="col-sm-4 control-label" style="padding-right:10px;">Usuario</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="username" name="username" placeholder="Usuario">
    </div>
  </div>
  <div class="form-group">
    <label for="password" class="col-sm-4 control-label" style="padding-right:10px;" >Contraseña</label>
    <div class=" col-sm-4">
      <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-4 col-sm-4 text-center">
      <button type="submit" class="btn btn-success active"><b>Iniciar Sesión</b></button>
    </div>
  </div>
  <br><br>



</form>




  </article>
  </section>
@endsection
