@extends('template.dashboard')

@section('css')

@endsection

@section('titulo')
  Panel Administrativo
@endsection

@section('content')
  <div class="jumbotron">

    @if(auth()->user()->tipo=='empleado')
      <h1>Bienvenido!</h1>
      <p>Mediante el registro que llevó a cabo podrá acceder a: </p>
      <p>*Información seleccionada de la empresa </p>
      <p>*Realizar trámites que facilitarán los servicios ofrecidos por la empresa.</p>
      <p>*De esta manera se conectará directamente con la empresa.</p>
      <p>
        {{--<a class="btn btn-lg btn-primary" href="../../components/#navbar" role="button">View navbar docs &raquo;</a>--}}
      </p>
    @else
      <h1>Bienvenido! Administrador</h1>
      <p>A travez de este sitema podrá acceder a: </p>
      <p>*Información seleccionada de los empleado registrado en aplicacion </p>
      <p>*Realizar trámites que facilitarán los servicios ofrecidos por la empresa a los empleado.</p>      
      <p>
    @endif


  </div>
@endsection
