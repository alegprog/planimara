@extends('template.dashboard')

@section('css')

@endsection

@section('titulo')
  Panel Administrativo
@endsection

@section('content')
<div class="well">
  <h1>Empleados</h1>
  <p class="text-right">
    <a href="{{url('/')}}/empleado/agregar-cedula" class="btn btn-success active">Agregar Cedula</a>
  </p>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th class="text-center">Cedula</th>
        <th class="text-center">Nombre y Apellido</th>
        <th class="text-center">Editar</th>
        <th class="text-center">Contrato</th>
        <th class="text-center">Cargar Nomina</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($empleados as $empleado)
        <tr>
          <td class="text-center">{{$empleado->cedula}}</td>
          <td class="text-center">{{$empleado->nombre}} {{$empleado->apellido}}</td>
          <td class="text-center">
            <a href="{{url('/')}}/empleado/{{$empleado->id}}/editar" title="Editar Empleado">
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
          </td>
          <td class="text-center">
            <a href="{{url('/')}}/empleado/{{$empleado->id}}/contrato" title="Información del empleado: (Sueldo, Cargo, Fecha Ingreso)">
              <i class="glyphicon glyphicon-list-alt"></i>
            </a>
          </td>
          <td class="text-center">
            <a href="{{url('/')}}/empleado/{{$empleado->id}}/trabajo" title="Información del empleado: (Horas Trabajadas)">
              <i class="glyphicon glyphicon-briefcase"></i>
          </td>
        <tr>
      @endforeach
    <tbody>
  </table>
</div>
@endsection
