@extends('template.dashboard')

@section('css')
  <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker/bootstrap-datepicker3.css')}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker/bootstrap-datepicker.standalone.css')}}">
@endsection

@section('js')
<script src="{{asset('js/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js')}}"></script>
<script>
  $('#input-daterange').datepicker({
    format: "dd/mm/yyyy",
    language: "es",
    autoclose: true
  });
</script>

@endsection

@section('titulo')
  Panel Administrativo
@endsection

@section('content')
<div class="well">
  <h3>Nomina empleado
    <span class="label label-default">Cedula: {{$empleado->cedula}}</span>
    <span class="label label-default">Nombre: {{$empleado->nombre}} {{$empleado->apellido}}</span>
    <span class="label label-warning pull-right">Sueldo: {{$contrato->monto}}</span>
  </h3>
  <hr>

  @if (session('status_error'))
      <div class="alert alert-danger">
        <b>Ha ocurrido un error,<br></b>
        {{ session('status_error') }}
      </div>
  @endif

  @if (session('status_warning'))
      <div class="alert alert-warning">
        <b>Ha ocurrido un error,<br></b>
        {{ session('status_warning') }}
      </div>
  @endif

  @if (session('status_success'))
      <div class="alert alert-success">
        <b>Excelente!<br></b>
        {{ session('status_success') }}
      </div>
  @endif

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <b>Ha ocurrido un error,<br></b>
      Por favor verifique los datos introducidos
    </div>
@endif


  <form class="form-horizontal" action="{{url('empleado/nomina/agregar')}}" method="post">
   {{ csrf_field() }}

   <div class="form-group col-sm-6">
     <label for="fecha_ingreso" class="col-sm-5 control-label">Fecha </label>
     <div class="col-sm-7">
       <div class="input-daterange input-group" id="input-daterange">

             <span class="input-group-addon" id="basic-addon3">Desde</span>
             <input type="text" class="form-control" name="fecha_desde" />
             <span class="input-group-addon " id="basic-addon3">Hasta</span>
              <input type="text" class="form-control input-block" name="fecha_hasta" />


       </div>
       @if ($errors->has('fecha_desde'))
           <span class="help-block">
             <strong class="text-danger"> {{ $errors->first('fecha_desde') }}</strong>
           </span>
       @endif
       @if ($errors->has('fecha_hasta'))
           <span class="help-block">
             <strong class="text-danger"> {{ $errors->first('fecha_hasta') }}</strong>
           </span>
       @endif

     </div>
   </div>

   <div class="clearfix"></div>

    <div class="form-group col-sm-6">
      <label for="dias_trabajados" class="col-sm-5 control-label">Dias Trabajados </label>
      <div class="col-sm-7">
        <input type="text" name="dias_trabajados" class="form-control" id="dias_trabajados" placeholder="Dias Trabajados" value="{{ old('dias_trabajados') }}">
        @if ($errors->has('dias_trabajados'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('dias_trabajados') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group col-sm-6">
      <label for="dias_de_descanso" class="col-sm-5 control-label">Dias de Descanso </label>
      <div class="col-sm-7">
        <input type="text" name="dias_de_descanso" class="form-control" id="dias_de_descanso" placeholder="Dias de Descanso" value="{{old('dias_de_descanso') ? old('dias_de_descanso'): 4 }}" >
        @if ($errors->has('dias_de_descanso'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('dias_de_descanso') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="form-group col-sm-6">
      <label for="sabados_trabajados" class="col-sm-5 control-label">Sabados Trabajados </label>
      <div class="col-sm-7">
        <input type="text" name="sabados_trabajados" class="form-control" id="sabados_trabajados" placeholder="Sabados Trabajados" value="{{old('sabados_trabajados')}}" >
        @if ($errors->has('sabados_trabajados'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('sabados_trabajados') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group col-sm-6">
      <label for="domingos_trabajados" class="col-sm-5 control-label">Domingo Trabajados </label>
      <div class="col-sm-7">
        <input type="text" name="domingos_trabajados" class="form-control" id="domingos_trabajados" placeholder="Domingos Trabajados" value="{{old('domingos_trabajados')}}" >
        @if ($errors->has('domingos_trabajados'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('domingos_trabajados') }}</strong>
            </span>
        @endif
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="form-group col-md-6">
      <label for="horas_extras" class="col-sm-5 control-label">Horas Extras </label>
      <div class="col-sm-7">
        <input type="text" name="horas_extras" class="form-control" id="horas_extras" placeholder="Horas Extras" value="{{old('horas_extras')}}" >
        @if ($errors->has('horas_extras'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('horas_extras') }}</strong>
            </span>
        @endif
      </div>
    </div>

    <div class="form-group col-md-6">
      <label for="otros_ingresos" class="col-sm-5 control-label">Otros Ingresos </label>
      <div class="col-sm-7">
        <input type="text" name="otros_ingresos" class="form-control" id="otros_ingresos" placeholder="Otros Ingresos" value="{{old('otros_ingresos')}}" >
        @if ($errors->has('otros_ingresos'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('otros_ingresos') }}</strong>
            </span>
        @endif
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="form-group col-md-6">
      <label for="prestamos_o_adelantos" class="col-sm-5 control-label">Pestamos / Adelantos </label>
      <div class="col-sm-7">
        <input type="text" name="prestamos_o_adelantos" class="form-control" id="prestamos_o_adelantos" placeholder="Pestamos / Adelantos" value="{{old('prestamos_o_adelantos')}}" >
        @if ($errors->has('prestamos_o_adelantos'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('prestamos_o_adelantos') }}</strong>
            </span>
        @endif
      </div>
    </div>

    <div class="form-group col-md-6">
      <label for="otras_deducciones" class="col-sm-5 control-label">Otras Deduciones </label>
      <div class="col-sm-7">
        <input type="text" name="otras_deducciones" class="form-control" id="otras_deducciones" placeholder="Otras Deducciones" value="{{old('otras_deducciones')}}" >
        @if ($errors->has('otras_deducciones'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('otras_deducciones') }}</strong>
            </span>
        @endif
      </div>
    </div>

    <div class="clearfix"></div>
    <hr>
    <div class="form-group">
      <div class="col-sm-offset-8 col-sm-3 text-right">
        <input type="hidden" name="id" value="{{$contrato->id}}" />
        <input type="hidden" name="usuario_id" value="{{$empleado->id}}" />
        <button type="submit" class="btn btn-success active">Guardar Nomina</button>
      </div>
    </div>
  </form>

</div>
@endsection
