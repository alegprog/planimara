@extends('template.dashboard')

@section('css')
  <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker/bootstrap-datepicker3.css')}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker/bootstrap-datepicker.standalone.css')}}">
@endsection

@section('js')
<script src="{{asset('js/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js')}}"></script>
<script>
  $('.fecha_ingreso').datepicker({
        format: "dd/mm/yyyy",
        startView: 2,
        language: "es",
        autoclose: true
  });
</script>

@endsection

@section('titulo')
  Panel Administrativo
@endsection

@section('content')
<div class="well">
  <h3>Contrato empleado
    <span class="label label-default">Cedula: {{$empleado->cedula}}</span>
    <span class="label label-default">Nombre: {{$empleado->nombre}} {{$empleado->apellido}}</span>
  </h3>
  <hr>

  @if (session('status_error'))
      <div class="alert alert-danger">
        <b>Ha ocurrido un error,<br></b>
        {{ session('status_error') }}
      </div>
  @endif

  @if (session('status_success'))
      <div class="alert alert-success">
        <b>Excelente!<br></b>
        {{ session('status_success') }}
      </div>
  @endif

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <b>Ha ocurrido un error,<br></b>
      Por favor verifique los datos introducidos
    </div>
@endif


  <form class="form-horizontal" action="{{url('empleado/contrato/actualizar')}}" method="post">
   {{ csrf_field() }}


    <div class="form-group">
      <label for="cargo" class="col-sm-2 control-label">Cargo </label>
      <div class="col-sm-3">
        <input type="text" name="cargo" class="form-control" id="oficina" placeholder="Cargo" value="{{ old('cargo') ? old('cargo') : $contrato->cargo }}">
        @if ($errors->has('cargo'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('cargo') }}</strong>
            </span>
        @endif
      </div>
    </div>



    <div class="form-group">
      <label for="oficina" class="col-sm-2 control-label">Oficina </label>
      <div class="col-sm-3">
        <input type="text" name="oficina" class="form-control" id="oficina" placeholder="Oficina" value="{{old('oficina')?old('oficina'):$contrato->oficina}}" >
        @if ($errors->has('oficina'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('oficina') }}</strong>
            </span>
        @endif
      </div>
    </div>


    <div class="form-group">
      <label for="sueldo" class="col-sm-2 control-label">Sueldo </label>
      <div class="col-sm-3">
        <input type="text" name="sueldo" class="form-control" id="sueldo" placeholder="Sueldo" value="{{old('sueldo')?old('sueldo'):$contrato->monto}}" >
        @if ($errors->has('sueldo'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('sueldo') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group">
      <label for="fecha_ingreso" class="col-sm-2 control-label">Fecha Ingreso </label>
      <div class="col-sm-3">
        <input type="date" name="fecha_ingreso" class="form-control fecha_ingreso" id="fecha_ingreso" placeholder="dd/mm/aaaa" value="{{ old('fecha_ingreso') ? old('fecha_ingreso') : $contrato->fecha_ingreso }}" >
        @if ($errors->has('fecha_ingreso'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('fecha_ingreso') }}</strong>
            </span>
        @endif
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <input type="hidden" name="id" value="{{$contrato->id}}" />
        <input type="hidden" name="usuario_id" value="{{$empleado->id}}" />
        <button type="submit" class="btn btn-success active">Modificar</button>
      </div>
    </div>
  </form>

</div>
@endsection
