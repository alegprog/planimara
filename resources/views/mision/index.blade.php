@extends('template.page')

@section('css')
  <link href="{{url('/')}}/css/estilos/estilosmision.css" rel="stylesheet">
@endsection

@section('titulo')
  Misión
@endsection

@section('content')
  <section class="articles">
  <article><br>
  <h2> Misión</h2>
  <img src="{{url('/')}}/imagenes/polaroidlogo.png" align="right" width="180px">
  <p>   Somos una Empresa del Estado Venezolano Socialista, orientada a la satisfacción de las necesidades humanas, básicas y productivas, mediante la prestación de servicios que contribuyen con el desarrollo socioagricola a nivel regional y nacional, basados en el modelo productivo socialista que busca la igualdad y la participación colectiva, mediante el uso racional de los recursos hidráulico, con equipos de alta tecnología al servicio del poder popular, como parte de una nueva estructura social incluyente. <br>
  <br> <br>   </p>

  </article>
  </section>
@endsection
