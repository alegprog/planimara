@extends('template.page')

@section('css')
  <link href="{{url('/')}}/css/estilos/estiloservicios.css" rel="stylesheet">
@endsection

@section('titulo')
  Servicios
@endsection

@section('content')
  <section class="articles">
    <article><h2> Servicios</h2>
    <p> *Perforación y saneamiento de pozos. <br>
      *sistemas de riego. <br>
      *Hojas Cartográficas. <br>
      *Venta de producción de la parcela demostrativa, cachamas, ovejos y vegetales.<br>
        Esto se logra a través de la oficina de Gerencia Técnica y de Ejecución de Proyectos </p>

  </article>
  </section>
@endsection
