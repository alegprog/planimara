<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="description" content="">
  <meta name="author" content="">
    <link href="{{url('/')}}/imagenes/favicon.ico.ico.ico" type="image/x-icon" rel="shortcut icon" />

  <!-- Bootstrap core CSS -->
  <link href="{{url('/')}}/css/bootstrap.min.css" rel="stylesheet">
        <title>Planimara - Pagina no encontrada.</title>

        <style>

        .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6,
        .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
            background-color: #FFF;
        }

        .padding{
          padding: 50px;
        }

        .error-details{
          padding: 10px;
          font-size: 16px;
        }

        h1,h2{
          font-weight: bold;
        }

        </style>
    </head>
    <body>
      <br><br>
<div class="container text-center padding">
    <div class="row" style="background:#FFF !important;">
        <div class="col-md-12">
            <div class="error-template padding">
              <img class="text-center" src="{{url('/')}}/imagenes/principal.png" width="997" height="116">

                <h1>
                    Oops!</h1>

                <h2>
                  Pagina no encontrada</h2>
                <div class="error-details">
                    Lo sentimos, se ha producido un error. No se ha encontrado la página solicitada.
                </div>
                <div class="error-actions">
                    <a href="{{url('/')}}/panel" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
                        Llévame a inicio </a> &nbsp;&nbsp; <a href="{{url('/')}}/contacto" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-envelope"></span> Soporte de contacto </a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{url('/')}}/js/j.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{url('/')}}/js/bootstrap.min.js"></script>
@yield('js')
</body>
</html>
