@extends('template.page')

@section('css')
  <link href="{{url('/')}}/css/estilos/estilosvision.css" rel="stylesheet">
@endsection

@section('titulo')
  Visión
@endsection

@section('content')
  <section class="articles">
  <article><br>
    <h2> Visión<br>
   <img src="{{url('/')}}/imagenes/polaroidlogo.png" align="right" width="180px"> 
  </h2>
  <p>
   Ser una empresa socialista que contribuya con la soberanía agroalimentaria de la nación, mediante el fortalecimiento del desarrollo agrícola socioproductivo, que impulse la capacidad de producción de alimentos, basado en los valores y principios humanista de equidad e inclusión social, y en la satisfacción de las necesidades sociales de la población de forma sustentable .<br><br><br> <br>  <br>

      </p>

  </article>
  </section>
@endsection
