@extends('template.page')

@section('css')
  <link href="{{url('/')}}/css/estilos/cedulaestilos.css" rel="stylesheet">
  <style>
    .slider {
      margin-bottom: 0px;
      padding-bottom: 0px;
    }
    .slider ul{
      margin-bottom: 0px;
      padding-bottom: 0px;
    }
    .slider li{
      margin-bottom: 0px;
      padding-bottom: 0px;
    }
    .slider img{
      margin-bottom: 0px;
      padding-bottom: 0px;
    }
  </style>
@endsection

@section('titulo')
  Registro
@endsection

@section('content')
  <section class="articles">
    <article>
    @if (isset($cedula))
        <div class="alert alert-info">
        <b>Bienvenido al registro de empleado,<br></b>
         completa por favor todo los datos solicitado para completar tu registro
        </div>
      @endif
    <br>
    <h2 ><strong>Registro</strong> </h2>
    <br>
    <h3> Datos Básicos</h3>
        <br><br>

 <form action="{{url('/guardar')}}" method="post">
  {{ csrf_field() }}
 	<section id="formulario">
	<div style="text-align:center;">

      <table style="margin: 0 auto;" width="343" height="334" border="0" align="center">
        <tr>
          <th width="84" height="62"><div align="left">Nombre <br>
          </div></th>
          <td width="249"><input name="nombre" type="text"  placeholder="Nombre"  required value="{{ old('nombre')}}" />
            @if ($errors->has('nombre'))
                <span class="help-block">
                  <strong class="text-danger"> {{ $errors->first('nombre') }}</strong>
                </span>
            @endif
          </td>
        </tr>
        <tr>
          <th height="67" scope="row"><div align="left">Apellido</div></th>
          <td><input name="apellido" type="text"  placeholder="Apellido" required="required" value="{{ old('apellido')}}" >
            @if ($errors->has('apellido'))
                <span class="help-block">
                  <strong class="text-danger"> {{ $errors->first('apellido') }}</strong>
                </span>
            @endif
          </td>
        </tr>
        <tr>
          <th height="66" scope="row"><div align="left">Cédula</div></th>
          <td><input name="cedula" type="text"  placeholder="Cédula" required value="{{ isset($cedula) ? $cedula : old('cedula')}}" autocomplete="off" />
            @if ($errors->has('cedula'))
                <span class="help-block">
                  <strong class="text-danger"> {{ $errors->first('cedula') }}</strong>
                </span>
            @endif
          </td>
        </tr>
        <tr>
          <th height="54" scope="row"><div align="left">Usuario</div></th>
          <td><input name="usuario" type="text"  placeholder="usuario" required value="{{ old('usuario')}}"/>
            @if ($errors->has('usuario'))
                <span class="help-block">
                  <strong class="text-danger"> {{ $errors->first('usuario') }}</strong>
                </span>
            @endif
          </td>
        </tr>
        <tr>
          <th scope="row"><div align="left">Contraseña</div></th>
          <td><input name="password" type="text"  placeholder="Contraseña" required value="{{ old('password')}}"/>
            @if ($errors->has('password'))
                <span class="help-block">
                  <strong class="text-danger"> {{ $errors->first('password') }}</strong>
                </span>
            @endif
          </td>
        </tr>

      </table>
      <input type="hidden" name="cedula_principal" value="{{ isset($cedula) ? $cedula : old('cedula_principal')}}" />
      <p><input type="submit" value="Enviar"/></p>
      </div>
	</section>


</form>




  </article>
  </section>
@endsection
