@extends('template.page')

@section('css')
  <link href="{{url('/')}}/css/estilos/cedulaestilos.css" rel="stylesheet">
@endsection

@section('titulo')
  Verificar Cedula
@endsection

@section('content')
  <section class="articles">
    <article>
      @if (session('status_error'))
        <div class="alert alert-danger">
        <b>Ofrecemos disculpas,<br></b>
        {{ session('status_error') }}
        </div>
      @endif
    <br>
    <p> Para poder llevar a cabo su registro, debe ingresar su número de cédula,<br> ésto es para comprobar que usted pertenece al personal de Planimara. </p>
    <section id="formulario">
     <p>
       <form action="{{url('/validar-cedula')}}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
            <br><br>
            <p>
              <label for="cedula">Numero de cedula </label>
              <input type="text"  autocomplete="off" autofocus class="form-control center-block text-center" id="cedula" required name="cedula" placeholder ="Escribe tu cedula"></p>
            </div>
            <p>
              <input type="submit" value="Enviar"/>
            </p>
        </form>
      </p>
    </section>

  </article>
  </section>
@endsection
