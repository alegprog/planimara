@extends('template.dashboard')

@section('css')

@endsection

@section('titulo')
  Panel Administrativo
@endsection

@section('content')
<div class="well">
  <h1>Recibos de pago</h1>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Fecha Desde</th>
        <th>Fecha Hasta</th>
        <th>Dias Trab.</th>
        <th>Dias Desc.</th>
        <th>Sab Trab.</th>
        <th>Dom Trab.</th>
        <th>Hrs Extras.</th>
        <th>Otros Ingreso.</th>
        <th>Acción</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($recibos as $recibo)
        <tr>
          <td>{{$recibo->fecha_desde}}</td>
          <td>{{$recibo->fecha_hasta}}</td>
          <td>{{$recibo->dias_trabajados}}</td>
          <td>{{$recibo->dias_de_descanso}}</td>
          <td>{{$recibo->sabados_trabajados}}</td>
          <td>{{$recibo->domingos_trabajados}}</td>
          <td>{{$recibo->horas_extras}}</td>
          <td>{{$recibo->otros_ingresos}}</td>
          <td>
            <a href="{{url('/')}}/recibos-pagos/{{$recibo->id}}/ver" title="Ver Recibo de pago">
              <i class="glyphicon glyphicon-eye-open"></i>
            </a>
            &nbsp;
            <a href="{{url('/')}}/recibos-pagos/{{$recibo->id}}/imprimir" target="_blank" title="Imprimir Recibo de pago">
              <i class="glyphicon glyphicon-print"></i>
            </a>
          </td>
        <tr>
      @endforeach
    <tbody>
  </table>
</div>
@endsection
