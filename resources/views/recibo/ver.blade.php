@extends('template.dashboard')

@section('css')

@endsection

@section('titulo')
  Panel Administrativo
@endsection

@section('content')
<div style="background-color:#FFF;padding:20px;" >
  <h1>Recibos de pago</h1>
  <table style="width: 200px;" class="table table-bordered pull-right">
      <thead>
        <tr>
          <th class="text-center">Desde</th>
          <th class="text-center">{{$recibo->fecha_desde}}</th>
        </tr>
        <tr>
          <th class="text-center">Hasta</th>
          <th class="text-center">{{$recibo->fecha_hasta}}</th>
        </tr>
      </thead>
  </table>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th class="text-center">Nombre del Trabajador</th>
        <th class="text-center">Cedula</th>
        <th class="text-center">Cargo</th>
        <th class="text-center">Salario Diario</th>
        <th class="text-center">Fecha Ingreso</th>
      </tr>
    </thead>
    <tbody>
        <tr>
          <td class="text-center">{{$empleado->nombre}} {{$empleado->apellido}}</td>
          <td class="text-center">{{$empleado->cedula}}</td>
          <td class="text-center">{{$contrato->cargo}}</td>
          <td class="text-center">{{round($resultado['sueldo_diario'],2)}}</td>
          <td class="text-center">{{$contrato->fecha_ingreso}}</td>
        <tr>
  </table>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Detalle de Concepto</th>
        <th class="text-center">Cantidad</th>
        <th class="text-center">Rata</th>
        <th class="text-center">Asignaciones</th>
        <th class="text-center">Deducciones</th>
      </tr>
    </thead>
    <tbody>
        <tr>
          <td>Dias Trabajados</td>
          <td class="text-center">{{$recibo->dias_trabajados}}</td>
          <td class="text-right">{{$resultado['sueldo_diario']}}</td>
          <td class="text-right">{{round($resultado['total_dias_trabajadas'],2)}}</td>
          <td></td>
        <tr>
        <tr>
          <td>Dias de Descanso</td>
          <td class="text-center">{{$recibo->dias_de_descanso}}</td>
          <td class="text-right">{{round($resultado['sueldo_diario'],2)}}</td>
          <td class="text-right">{{round($resultado['total_dias_de_descanso'],2)}}</td>
          <td></td>
        <tr>
        <tr>
          <td>Sabados Trabajados</td>
          <td class="text-center">{{$recibo->sabados_trabajados}}</td>
          <td class="text-right">{{round($resultado['sueldo_por_hora_fines_semana'],2)}}</td>
          <td class="text-right">{{round($resultado['total_sabados_trabajados'],2)}}</td>
          <td></td>
        <tr>
        <tr>
          <td>Domingos Trabajados</td>
          <td class="text-center">{{$recibo->domingos_trabajados}}</td>
          <td class="text-right">{{round($resultado['sueldo_por_hora_fines_semana'],2)}}</td>
          <td class="text-right">{{round($resultado['total_domingos_trabajados'],2)}}</td>
          <td></td>
        <tr>
        <tr>
          <td>Horas Extras</td>
          <td class="text-center">{{$recibo->horas_extras}}</td>
          <td class="text-right">{{round($resultado['sueldo_horas_extras'],2)}}</td>
          <td class="text-right">{{round($resultado['total_horas_extras'],2)}}</td>
          <td></td>
        <tr>
        <tr>
          <td>Otros Ingresos</td>
          <td></td>
          <td></td>
          <td class="text-right">{{round($resultado['total_horas_extras'],2)}}</td>
          <td></td>
        <tr>
        <tr>
          <td>Retencion L.P.H</td>
          <td></td>
          <td></td>
          <td></td>
          <td class="text-right">{{round($resultado['total_retencion_lph'],2)}}</td>
        <tr>
        <tr>
          <td>Retencion P.F</td>
          <td></td>
          <td></td>
          <td></td>
          <td class="text-right">{{round($resultado['total_retencion_pf'],2)}}</td>
        <tr>
        <tr>
          <td>Retencion S.S.O</td>
          <td></td>
          <td></td>
          <td></td>
          <td class="text-right">{{round($resultado['total_retencion_sos'],2)}}</td>
        <tr>
        <tr>
          <td>Prestamo/Adelantos</td>
          <td></td>
          <td></td>
          <td></td>
          <td class="text-right">{{round($resultado['total_prestamo_adelanto'],2)}}</td>
        <tr>
        <tr>
          <td>Otras Deducciones</td>
          <td></td>
          <td></td>
          <td></td>
          <td class="text-right">{{round($resultado['total_otras_deducciones'],2)}}</td>
        <tr>
        <tr>
          <td>Total Asignaciones/Deducciones</td>
          <td></td>
          <td></td>
          <td class="text-right">{{round($resultado['total_acumulado'],2)}}</td>
          <td class="text-right">{{round($resultado['total_retenciones'],2)}}</td>
        <tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td class="text-right"><b>NETO A COBRAR</b></td>
          <td class="text-right"><b>{{round($resultado['total'],2)}}</b></td>
        <tr>

    <tbody>
  </table>

  <br>
<h2 class="text-right">
  <a href="{{url('/')}}/recibos-pagos/{{$recibo->id}}/imprimir" target="_blank" title="Imprimir Recibo de pago">
    <i class="glyphicon glyphicon-print"></i>
  </a>
</h2>
</div>
@endsection
