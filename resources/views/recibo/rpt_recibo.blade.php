<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Recibo de Pago</title>
    <style>
      body{
        font-family: sans-serif;
      }
      .f-50{
        font-size: 50px !important;
      }
      .f-35{
        font-size: 35px !important;
      }
      .f-20{
        font-size: 20px !important;
      }
      .f-25{
        font-size: 25px !important;
      }
      .f-30{
        font-size: 30px !important;
      }
      .f-12{
        font-size: 12px !important;
      }

      img{
        text-align: center;
      }
      h1{
        text-align: center;
        padding: 2px !important;
        margin: 0px !important;
      }
      p{
        text-align: center;
        padding: 2px !important;
        margin: 0px !important;
      }

      .border{
        border:1px solid #000;
        border-collapse: collapse;
      }

      .text-center{
        text-align: center;
      }

      .text-right{
        text-align: right;
      }

      .table{
        border:1px solid #000;
        border-collapse: collapse;
        width: 100%;
      }

      .table th{
        border:1px solid #000;
        border-collapse: collapse;
      }

      .table td{
        border:1px solid #000;
        border-collapse: collapse;
      }

    </style>

  </head>
  <body>

<div style="text-align:center;">
  <br>
  <img width="500" style="text-align:center;" src="{{'data:image/' . $type_image . ';base64,' .$logo}}"  >
  <br><br><br><br>
  <h1 class="text-center f-35 p-t-0">Recibo de Pago</h1>
  <br><br><br>
  <table style="width: 200px;right:0;position:absolute;" class="text-center border">
      <thead >
        <tr>
          <th class="text-center border" >Desde</th>
          <th class="text-center border">{{$recibo->fecha_desde}}</th>
        </tr>
        <tr class="border">
          <th class="text-center border">Hasta</th>
          <th class="text-center border">{{$recibo->fecha_hasta}}</th>
        </tr>
      </thead>
  </table><br><br><br>
  <table class="table">
    <thead>
      <tr >
        <th class="text-center">Nombre del Trabajador</th>
        <th class="text-center">Cedula</th>
        <th class="text-center">Cargo</th>
        <th class="text-center">Salario Diario</th>
        <th class="text-center">Fecha Ingreso</th>
      </tr>
    </thead>
    <tbody>
        <tr>
          <td class="text-center">{{$empleado->nombre}} {{$empleado->apellido}}</td>
          <td class="text-center">{{$empleado->cedula}}</td>
          <td class="text-center">{{$contrato->cargo}}</td>
          <td class="text-center">{{round($resultado['sueldo_diario'],2)}}</td>
          <td class="text-center">{{$contrato->fecha_ingreso}}</td>
        </tr>
     </tbody>
  </table>
  <br>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Detalle de Concepto</th>
        <th class="text-center">Cantidad</th>
        <th class="text-center">Rata</th>
        <th class="text-center">Asignaciones</th>
        <th class="text-center">Deducciones</th>
      </tr>
    </thead>
    <tbody>
        <tr>
          <td>Dias Trabajados</td>
          <td class="text-center">{{$recibo->dias_trabajados}}</td>
          <td class="text-right">{{$resultado['sueldo_diario']}}</td>
          <td class="text-right">{{round($resultado['total_dias_trabajadas'],2)}}</td>
          <td></td>
        </tr>
        <tr>
          <td>Dias de Descanso</td>
          <td class="text-center">{{$recibo->dias_de_descanso}}</td>
          <td class="text-right">{{round($resultado['sueldo_diario'],2)}}</td>
          <td class="text-right">{{round($resultado['total_dias_de_descanso'],2)}}</td>
          <td></td>
        </tr>
        <tr>
          <td>Sabados Trabajados</td>
          <td class="text-center">{{$recibo->sabados_trabajados}}</td>
          <td class="text-right">{{round($resultado['sueldo_por_hora_fines_semana'],2)}}</td>
          <td class="text-right">{{round($resultado['total_sabados_trabajados'],2)}}</td>
          <td></td>
        </tr>
        <tr>
          <td>Domingos Trabajados</td>
          <td class="text-center">{{$recibo->domingos_trabajados}}</td>
          <td class="text-right">{{round($resultado['sueldo_por_hora_fines_semana'],2)}}</td>
          <td class="text-right">{{round($resultado['total_domingos_trabajados'],2)}}</td>
          <td></td>
        </tr>
        <tr>
          <td>Horas Extras</td>
          <td class="text-center">{{$recibo->horas_extras}}</td>
          <td class="text-right">{{round($resultado['sueldo_horas_extras'],2)}}</td>
          <td class="text-right">{{round($resultado['total_horas_extras'],2)}}</td>
          <td></td>
        </tr>
        <tr>
          <td>Otros Ingresos</td>
          <td></td>
          <td></td>
          <td class="text-right">{{round($resultado['total_horas_extras'],2)}}</td>
          <td></td>
        </tr>
        <tr>
          <td>Retencion L.P.H</td>
          <td></td>
          <td></td>
          <td></td>
          <td class="text-right">{{round($resultado['total_retencion_lph'],2)}}</td>
        </tr>
        <tr>
          <td>Retencion P.F</td>
          <td></td>
          <td></td>
          <td></td>
          <td class="text-right">{{round($resultado['total_retencion_pf'],2)}}</td>
        </tr>
        <tr>
          <td>Retencion S.S.O</td>
          <td></td>
          <td></td>
          <td></td>
          <td class="text-right">{{round($resultado['total_retencion_sos'],2)}}</td>
        </tr>
        <tr>
          <td>Prestamo/Adelantos</td>
          <td></td>
          <td></td>
          <td></td>
          <td class="text-right">{{round($resultado['total_prestamo_adelanto'],2)}}</td>
        </tr>
        <tr>
          <td>Otras Deducciones</td>
          <td></td>
          <td></td>
          <td></td>
          <td class="text-right">{{round($resultado['total_otras_deducciones'],2)}}</td>
        </tr>
        <tr>
          <td>Total Asignaciones/Deducciones</td>
          <td></td>
          <td></td>
          <td class="text-right">{{round($resultado['total_acumulado'],2)}}</td>
          <td class="text-right">{{round($resultado['total_retenciones'],2)}}</td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td class="text-right"><b>NETO A COBRAR</b></td>
          <td class="text-right"><b>{{round($resultado['total'],2)}}</b></td>
        </tr>

    <tbody>
  </table>



  <br> <br> <br> <br> <br>

  <div class="text-center" style="text-align:center;border-top:1px solid #000;margin-left:180px;margin-right:180px;"></div>
  <p class="text-center">{{$empleado->nombre}} {{$empleado->apellido}}</p>
  <p class="text-center">Recibe Conforme</p>
  <br> <br>
  <p class="text-center f-12">Dirección: Calle 3, con Av. 18 y 19, Edif. Planimara, Sierra Maestra, San Francisco</p>
  <p class="text-center f-12">Número Telefónico: +58 261- 758.15.47</p>
  <p class="text-center f-12">Correo Electrónico: informatica@planima.gob.ve

</p>
<div>
</body>
</html>
