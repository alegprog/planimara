@extends('template.dashboard')

@section('css')
  <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker/bootstrap-datepicker3.css')}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker/bootstrap-datepicker.standalone.css')}}">
@endsection

@section('js')
<script src="{{asset('js/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js')}}"></script>
<script>

</script>

@endsection

@section('titulo')
  Panel Administrativo
@endsection

@section('content')
<div class="well">
  <h3>Agregar Cedula</h3>
  <hr>

  @if (session('status_error'))
      <div class="alert alert-danger">
        <b>Ha ocurrido un error,<br></b>
        {{ session('status_error') }}
      </div>
  @endif

  @if (session('status_success'))
      <div class="alert alert-success">
        <b>Excelente!<br></b>
        {{ session('status_success') }}
      </div>
  @endif

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <b>Ha ocurrido un error,<br></b>
      Por favor verifique los datos introducidos
    </div>
@endif


  <form class="form-horizontal" action="{{url('empleado/agregar/cedula')}}" method="post">
   {{ csrf_field() }}


    <div class="form-group">
      <label for="sueldo" class="col-sm-2 control-label">Cedula </label>
      <div class="col-sm-3">
        <input type="text" name="cedula" class="form-control" id="cedula" placeholder="Cedula" value="{{old('cedula')}}" >
        @if ($errors->has('cedula'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('cedula') }}</strong>
            </span>
        @endif
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success active">Agregar</button>
      </div>
    </div>
  </form>

</div>
@endsection
