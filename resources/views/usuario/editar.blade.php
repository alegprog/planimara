@extends('template.dashboard')

@section('css')
  <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker/bootstrap-datepicker3.css')}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker/bootstrap-datepicker.standalone.css')}}">
@endsection

@section('js')
<script src="{{asset('js/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js')}}"></script>
<script>

</script>

@endsection

@section('titulo')
  Panel Administrativo
@endsection

@section('content')
<div class="well">
  <h3>Editar Empleado
    <span class="label label-default">Cedula: {{$empleado->cedula}}</span>
    <span class="label label-default">Nombre: {{$empleado->nombre}} {{$empleado->apellido}}</span>
  </h3>
  <hr>

  @if (session('status_error'))
      <div class="alert alert-danger">
        <b>Ha ocurrido un error,<br></b>
        {{ session('status_error') }}
      </div>
  @endif

  @if (session('status_success'))
      <div class="alert alert-success">
        <b>Excelente!<br></b>
        {{ session('status_success') }}
      </div>
  @endif

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <b>Ha ocurrido un error,<br></b>
      Por favor verifique los datos introducidos
    </div>
@endif


  <form class="form-horizontal" action="{{url('empleado/actualizar')}}" method="post">
   {{ csrf_field() }}


    <div class="form-group">
      <label for="cargo" class="col-sm-2 control-label">Nombre </label>
      <div class="col-sm-3">
        <input type="text" name=nombre class="form-control" id="nombre" placeholder="Nombre" value="{{ old('nombre') ? old('nombre') : $empleado->nombre }}">
        @if ($errors->has('empleado'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('empleado') }}</strong>
            </span>
        @endif
      </div>
    </div>



    <div class="form-group">
      <label for="oficina" class="col-sm-2 control-label">Apellido </label>
      <div class="col-sm-3">
        <input type="text" name="apellido" class="form-control" id="apellido" placeholder="Apellido" value="{{old('apellido') ? old('apellido') : $empleado->apellido}}" >
        @if ($errors->has('apellido'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('apellido') }}</strong>
            </span>
        @endif
      </div>
    </div>


    <div class="form-group">
      <label for="sueldo" class="col-sm-2 control-label">Cedula </label>
      <div class="col-sm-3">
        <input type="text" name="cedula" class="form-control" id="cedula" placeholder="Cedula" value="{{old('cedula')?old('cedula'): $empleado->cedula}}" >
        @if ($errors->has('cedula'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('cedula') }}</strong>
            </span>
        @endif
      </div>
    </div>

    <div class="form-group">
      <label for="sueldo" class="col-sm-2 control-label">Usuario </label>
      <div class="col-sm-3">
        <input type="text" name="usuario" class="form-control" id="usuario" placeholder="Usuario" value="{{old('usuario')?old('usuario'): $empleado->username}}" >
        @if ($errors->has('usuario'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('usuario') }}</strong>
            </span>
        @endif
      </div>
    </div>

    <div class="form-group">
      <label for="sueldo" class="col-sm-2 control-label">Contraseña </label>
      <div class="col-sm-3">
        <input type="password" name="password" class="form-control" id="password" placeholder="Contraseña" >
        @if ($errors->has('password'))
            <span class="help-block">
              <strong class="text-danger"> {{ $errors->first('password') }}</strong>
            </span>
        @endif
      </div>
    </div>


    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <input type="hidden" name="id" value="{{$empleado->id}}" />
        <button type="submit" class="btn btn-success active">Modificar</button>
      </div>
    </div>
  </form>

</div>
@endsection
