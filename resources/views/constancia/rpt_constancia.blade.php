<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Constancia de Trabajo</title>
    <style>
      body{
        font-family: sans-serif;
      }
      .f-50{
        font-size: 50px !important;
      }
      .f-35{
        font-size: 35px !important;
      }
      .f-20{
        font-size: 20px !important;
      }
      .f-25{
        font-size: 25px !important;
      }
      .f-30{
        font-size: 30px !important;
      }
      img{
        text-align: center;
      }
      h1{
        text-align: center;
        padding: 2px !important;
        margin: 0px !important;
      }
      p{
        text-align: center;
        padding: 2px !important;
        margin: 0px !important;
      }
    </style>

  </head>
  <body>

<div style="text-align:center;">
  <br>
  <img width="500" style="text-align:center;" src="{{'data:image/' . $type_image . ';base64,' .$logo}}"  >
  <br><br><br><br>
  <h1 class="text-center f-35 p-t-0">Constancia de Trabajo</h1>
  <br><br><br>
  <p style="margin-left:50px; margin-right:50px;font-size:18px; text-align:justify;" class="text-justify">
    Por medio de la presente, Nosotros PLANIMARA C.A, RIF N° G-20007664-0,
    hacemos contar que el <b class="text-uppercase">{{$empleado->nombre}} {{$empleado->apellido}}</b>, Portador de la Cedula de identidad
    N° {{$empleado->cedula}} presta sus servicio para esta empresa desde el dia {{$contrato->fecha_ingreso}}
    hasta la presente fecha, desempeñando el cargo de Ingeniero Civil,
    devengando un sueldo basico mensual de Bs. {{$contrato->monto > 0 ? f_convertNmro($contrato->monto) : '' }} (Bs. {{round($contrato->monto,2)}})
  </p>

  <br>

  <p style="margin-left:50px; margin-right:50px;font-size:18px; text-align:justify;" class="text-justify">
    Constancia que se expide a petición de las partes interesada,
    Maracaibo a los {{$date->format('j')}} dias del mes de {{$date->format('F')}} del {{$date->format('Y')}}.
  <p>



  <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <br>

  <div class="text-center" style="text-align:center;border-top:1px solid #000;margin-left:180px;margin-right:180px;"></div>
  <p class="text-center">ING. PASCUAL SANTILLI</p>
  <p class="text-center">GERENTE GENERAL</p>
<div>
</body>
</html>
