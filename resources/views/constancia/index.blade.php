@extends('template.dashboard')

@section('css')

@endsection

@section('titulo')
  Panel Administrativo
@endsection

@section('content')
<div style="background-color:#FFF;" >
  <br><br>
  <h1 class="text-center"><b>Constancia de Trabajo</b></h1>
  <br><br><br>
  <p style="margin-left:50px; margin-right:50px;font-size:18px;" class="text-justify">
    Por medio de la presente, Nosotros PLANIMARA C.A, RIF N° G-20007664-0,
    hacemos contar que el <b class="text-uppercase">{{$empleado->nombre}} {{$empleado->apellido}}</b>, Portador de la Cedula de identidad
    N° {{$empleado->cedula}} presta sus servicio para esta empresa desde el dia {{$contrato->fecha_ingreso}}
    hasta la presente fecha, desempeñando el cargo de Ingeniero Civil,
    devengando un sueldo basico mensual de Bs. {{$contrato->monto > 0 ? f_convertNmro($contrato->monto) : '' }} (Bs. {{$contrato->monto}})
  </p>

  <br>

  <p style="margin-left:50px; margin-right:50px;font-size:18px;" class="text-justify">
    Constancia que se expide a petición de las partes interesada,
    Maracaibo a los {{$date->format('j')}} dias del mes de {{$date->format('F')}} del {{$date->format('Y')}}.
  <p>

  <br>
  <br>
  <br>

  <h2 class="text-right" style="padding:20px;">
    <a href="{{url('/')}}/pdfconstancia" target="_blank" title="Imprimir Recibo de pago">
      <i class="glyphicon glyphicon-print"></i>
    </a>
  </h2>

</div>
@endsection
