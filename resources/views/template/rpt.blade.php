<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SG-Eventos</title>

    <!-- Bootstrap -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <style>
      body{

      }
    </style>

    <!-- Latest compiled and minified CSS -->
    <!--<link rel="stylesheet"
    href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
    crossorigin="anonymous">-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <br>
      <a id="boton" style="margin:2px;" class="btn btn-success pull-right" href="javascript:imprSelec('muestra')">
        <i class="glyphicon glyphicon-print"></i> Imprimir
      </a>

      <a id="boton" style="margin:2px;" class="btn btn-success pull-right" href="{{url(trans('sg_eventos.'.Route::currentRouteName(),['id'=>$id]))}}">
        PDF
      </a>
    </div>
    <div class="container">
      <div class="col-xs-12 text-center">
        <img src="{{asset('assets/image/logo_ujgh.gif')}}" width="500" >
        <br><br><br>
      </div>
    </div>

    @yield('content')


    <div class="col-md-6">

    </div>
    <div class="col-md-12">

    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{asset('assets/js/jquery/jquery.min.js')}}" ></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}" ></script>
    <script src="{{asset('assets/js/jsPDF/jspdf.debug.js')}}"></script>
    <script src="{{asset('assets/js/jsPDF/html2pdf.js')}}"></script>
    <script>
      function imprSelec(muestra)
      {
        //$('#boton').hide();
        //document.getElementById("boton").style.visibility = "hidden";
        print();
        /*var ficha=document.getElementById(muestra);
        var ventimp=window.open(' ','popimpr');
        ventimp.document.write(ficha.innerHTML);
        ventimp.document.close();
        ventimp.print();
        ventimp.close();*/
      }

      function imprimirPdf(){

        var pdf = new jsPDF('p', 'pt', 'letter');
    	  pdf.canvas.height = 72 * 11;
    	  pdf.canvas.width = 72 * 8.5;
    	  html2pdf(document.body, pdf, function(pdf){
            //var iframe = document.createElement('iframe');
            //iframe.setAttribute('style','position:absolute;right:0; top:0; bottom:0; height:100%; width:500px');
            //document.body.appendChild(iframe);
            //iframe.src = pdf.output('datauristring');
            pdf.save('nombre');
    	  });


        /*var doc = new jsPDF();

        // We'll make our own renderer to skip this editor
        var specialElementHandlers = {
        	'#editor': function(element, renderer){
        		return true;
        	}
        };

        // All units are in the set measurement for the document
        // This can be changed to "pt" (points), "mm" (Default), "cm", "in"
        doc.fromHTML($('body').get(0), 15, 15, {
        	'width': 170,
        	'elementHandlers': specialElementHandlers
        });*/


      }
    </script>

  </body>
</html>
