<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
      <link href="{{url('/')}}/imagenes/favicon.ico.ico.ico" type="image/x-icon" rel="shortcut icon" />

    <title>Planimara - @yield('titulo')</title>


    <!-- Bootstrap core CSS -->
    <link href="{{url('/')}}/css/bootstrap.min.css" rel="stylesheet">
    @yield('css')

    {{--
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="navbar-static-top.css" rel="stylesheet">
    --}}

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    {{--<script src="../../assets/js/ie-emulation-modes-warning.js"></script>--}}

    <style type="text/css">
      body {
        {{--min-height: 2000px;--}}
      }

      html {
  position: relative;
  min-height: 100%;
}

      body{

      }

      header{
        background-color: #FFF;
      }

      footer{
        background-color: #000 !important;
        color:#FFF;
      }

      .navbar-static-top {
        margin-bottom: 19px;
      }

      .footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        /* Set the fixed height of the footer here */
        height: 40px;
        background-color: #f5f5f5;
      }

      .footer > .container {
        padding-top: 10px;
        padding-right: 15px;
        padding-left: 15px;
      }

      .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5,
      .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12,
      .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9,
      .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5,
      .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12,
      .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {

            background-color: transparent !important;
      }

      .form-horizontal .control-label {
          padding-right: 7px;
      }

      .well{
        background: #FFF !important;
      }

      .navbar {
          min-height: 40px;
      }

      .navbar-default {
          background-color: green;
          border-color: #e7e7e7;
          color: #fff;
      }

      .navbar-default .navbar-nav > li > a:focus, .navbar-default .navbar-nav > li > a:hover {
          color: #FFF !important;
          background-color: transparent;
      }

      .navbar-default .navbar-nav > li > a {
          color: #FFF;
          font-weight: bold;
      }

      .navbar-nav > li > a {
          padding-top: 15px;
          padding-bottom: 15px;
      }



    </style>



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <header>
    <div class="cintillo">
    <img src="{{url('/')}}/imagenes/cintillo.jpg" width="100%" ></div>

    <div class="logotipo">
    <img src="{{url('/')}}/imagenes/principal.png" width="100%" height="116">  </div>

        <div class="fecha">
    <div class="fecha-portada" align="right"><font size="3" font color="green"> <script>
    var meses = new Array ("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");
    var f=new Date();
    document.write(f.getDate() + " | " + meses[f.getMonth()] + " | " + f.getFullYear());
    </script></font> </div>
    </div>
  </header>
    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" style="padding-top:20px;" href="{{url('/')}}"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="{{url('/')}}">Web</a></li>
            <li><a href="{{url('/')}}/panel">Panel</a></li>
            @if(auth()->user()->tipo=='administrador')
              <li><a href="{{url('/')}}/empleados">Empleado</a></li>
            @endif
            @if(auth()->user()->tipo=='empleado')
              <li><a href="{{url('/')}}/recibos-pagos">Recibos/Pagos</a></li>
              <li><a href="{{url('/')}}/constancia">Constancia</a></li>
            @endif
            {{--<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>--}}
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{url('/')}}/perfil">Perfil: {{auth()->user()->nombre}} {{auth()->user()->apellido}}</a></li>
            <li><a href="{{url('/')}}/logout">Cerrar Sesión</a></li>
            {{--<li><a href="../navbar-fixed-top/">Fixed top</a></li>--}}
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


    <div class="container">

      <!-- Main component for a primary marketing message or call to action -->
      @yield('content')

      <br><br><br>

    </div> <!-- /container -->


    <footer class="footer">
      <div class="container">
        <p class="text-muted text-center"> &copy;  Planimara RIF G200076640 </p>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{url('/')}}/js/j.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{url('/')}}/js/bootstrap.min.js"></script>
    @yield('js')
  </body>
</html>
