<!doctype html>
<html lang="es">
<head >
    <!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
< ![endif]-->
    <link href="{{url('/')}}/imagenes/favicon.ico.ico.ico" type="image/x-icon" rel="shortcut icon" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta charset="utf-8">
<title>Planimara - @yield('titulo')</title>
<left>
<!--lo nuevo -->
 <!-- Bootstrap -->
        <link href="{{url('/')}}/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{url('/')}}/css/font-awesome.css" rel="stylesheet">

        <!--<link href="{{url('/')}}/css/estilos.css" rel="stylesheet" type="text/css">-->
        @yield('css')
<link href="{{url('/')}}/css/font-awesome.css" rel="stylesheet">





 <!--lo nuevo -->
</left>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
<style type="text/css">
   body { background: #ccc !important; } /* Adding !important forces the browser to overwrite the default style applied by Bootstrap */
</style>





<body>
<section id="general">
<header>
<div class="cintillo">
<img src="{{url('/')}}/imagenes/cintillo.jpg"></div>

<div class="logotipo">
<img src="{{url('/')}}/imagenes/principal.png" width="997" height="116">  </div>

    <div class="fecha">
<div class="fecha-portada" align="right"><font size="3" font color="green"> <script>
var meses = new Array ("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");
var f=new Date();
document.write(f.getDate() + " | " + meses[f.getMonth()] + " | " + f.getFullYear());
</script></font> </div>
</div>

<ul class="menu">

<li> <a href="{{url('/')}}" > <span class="fa fa-home" ></span> Inicio </a> </li>
<li> <a href="" > <span class="fa fa-institution"></span> Empresa </a>
<ul >
<li> <a href="{{url('mision')}}">   Misión </a></li>
<li> <a href="{{url('vision')}}">   Visión</a></li>
<li> <a href="{{url('objetivos')}}"> Objetivos </a></li>
</ul>
</li>
<li> <a href="{{url('servicios')}}" > <span class="fa fa-suitcase"></span> Servicios</a> </li>
<li> <a href="{{url('contacto')}}"> <span class="fa fa-envelope"></span>  Contacto </a> </li>
	 <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          @if (Auth::check())
              <li><a href="{{ url('/panel') }}">Panel</a></li>
              <li><a href="{{ url('/logout') }}">Cerrar sessión</a></li>
          @else
            <li><a href="{{url('login')}}" ><span class="glyphicon glyphicon-user" aria-hidden="true">  </span> Acceder</a></li>
            <li><a href="{{url('verificar-cedula')}}" ><span class="glyphicon glyphicon-pencil" aria-hidden="true">  </span> Registrarse </a></li>
          @endif
        </ul>
   </div>
</ul>
<div class="slider">
<ul>
<li> <img src="{{url('/')}}/imagenes/02.jpg" alt=""></li>
<li><img src="{{url('/')}}/imagenes/05.jpg" alt=""></li>
<li><img src="{{url('/')}}/imagenes/06.jpg" alt=""></li>
<li><img src="{{url('/')}}/imagenes/07.jpg" alt""></li>
<li><img src="{{url('/')}}/imagenes/08.jpg"alt""></li>
<li><img src="{{url('/')}}/imagenes/09.jpg" alt=""></li>
</ul>
</div>
</header>




<section class="main">

  @yield('content')

		   <div  align="center"  id="footer-contenedor" >
<div  id="footer-banner-1">
<div class="zoom">
<a href="http://www.iclam.gov.ve/"  target="_blank"><img class="zoom" src="{{url('/')}}/imagenes/iclam.png" alt="Instituto para el Control y la conservación de la cuenca del Lago De Maracaibo" width="95" height="52"  title="Instituto para el Control y la conservación de la cuenca del Lago De Maracaibo" align="baseline"> </a></div></div>

<div id="footer-banner-2">
<div class="zoom">
<a href="http://www.mat.gob.ve/"  target="_blank"><img  title="Ministerio de Agricultura y Tierras" alt="Ministerio de Agricultura y Tierras" src="{{url('/')}}/imagenes/mat.png" width="107" height="46"> </a></div></div>

<div id="footer-banner-3">
<div class="zoom">
<a href="http://www.corpozulia.gob.ve/"  target="_blank"><img  title="Corpozulia" alt="Corpozulia" src="{{url('/')}}/imagenes/corpozulia.gif" width="124" height="60"> </a></div></div>

<div id="footer-banner-4">
<div class="zoom">
<a href="http://www.hidrolago.gov.ve/"  target="_blank"><img  title="Hidrolago" alt="Hidrolago" src="{{url('/')}}/imagenes/hidrolagoo.png" width="106" height="65"> </a></div></div>

<div id="footer-banner-5">
<div class="zoom">
<a href="http://www.zulia.gob.ve/"  target="_blank"><img  title="Gobernación Bolivariana Del Zulia" alt="Gobernación Bolivariana Del Zulia" src="{{url('/')}}/imagenes/gobernacion.png" width="137" height="65"> </a></div></div>
<div id="footer-banner-6">
<div class="zoom">
<a href="http://www.minea.gob.ve/"  target="_blank"><img  title="Ministerio Del Poder Popular para el Ecosocialismo y Aguas" alt="Ministerio Del Poder Popular para el Ecosocialismo y Aguas" src="{{url('/')}}/imagenes/minea.png" width="108" height="55"></a></div></div>
</div>
</section>
<footer> &copy;  Planimara RIF G200076640 </footer>
</section>
</body>

</html>
<!--lo nuevo  -->

   <div class="modal fade" id="modallogin">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">

                        <h2>Bienvenido/a</h2>
                    </div>
                    <div class="modal-body">
                        <form action="validacion.php" method="post">
                            <div class="form-group">
                                <label for="inputName">Nombre de usuario
                                </label>
                                <input type="text" autofocus class="form-control" id="inputName" required name="usuario" placeholder="Escribe tu usuario">
                            </div>
                            <div class="form-group">
                                <label for="inputPassword">
                                    Contraseña
                                </label>
                                <input type="password" class="form-control" required id="inputPassword" name="contraseña" placeholder="Escribe tu contraseña">
                            </div>
                            <button type="submit" class="btn btn-success btn-block">Iniciar sesion</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!--end modal>





<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="{{url('/')}}/js/j.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{url('/')}}/js/bootstrap.min.js"></script>
    </body>
</html>
