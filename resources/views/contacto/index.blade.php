@extends('template.page')

@section('css')
  <link href="{{url('/')}}/css/estilos/estiloscontacto.css" rel="stylesheet">
@endsection

@section('titulo')
  Contacto
@endsection

@section('content')
  <section class="articles">
    <article>
<div  class="dos-columnas">

    <p><h2>Ubicación</h2><br> <iframe marginheight="8" marginwidth="8"src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3921.809007806211!2d-71.63235818520077!3d10.594100892445384!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e8997706aa0d1fb%3A0x49448274fdf98e29!2sSierra+Maestra%2C+Maracaibo%2C+Zulia!5e0!3m2!1ses-419!2sve!4v1475456607921" width="400" height="300" align="right"frameborder="15" border-right="8" style="border:10" allowfullscreen></iframe>
    <br>
   </p>

       <p> <h2>Contáctenos</h2>
          <h3>
           <strong>Correo Electrónico</strong><br>
            <a href="mailto:maracaibo@psm.edu.ve">informatica@planima.gob.ve</a><br>
            <br>
            <strong>Números Telefónicos</strong><br>
            <p>+58 261-741.55.27 <br>
            +58 261-753.85.77 <br>
            <br>Fax: +58 261- 793.46.71<br>
            <br>Directo al Área de Recursos Humanos:<br>
         +58 261- 758.15.47<br></p>
            <br></h3></p><br>
    </div>

  </article>
  </section>
@endsection
