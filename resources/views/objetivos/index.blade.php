@extends('template.page')

@section('css')
  <link href="{{url('/')}}/css/estilos/estilos.objetivos.css" rel="stylesheet">
@endsection

@section('titulo')
  Objetivos
@endsection

@section('content')
  <section class="articles">
  <article><br>
    <h2> Objetivos De Planimara<br>
   <img src="{{url('/')}}/imagenes/polaroidlogo.png" align="right" width="180px">
  </h2>
  <p>
  *Fomentar el trabajo productivo para el manejo racional de los recursos hidráulicos, con el desarrollo de un modelo endógeno con la participación de los productores y comunidades, como base de la construcción del socialismo.<br>
  *Rescatar los valores y la ética humanista como base de la nueva moral colectiva.<br>
  *Elevar los niveles de equidad, eficacia, eficiencia y calidad de la acción como servidores públicos.<br>
  *Asegurar la eficiencia en sus procesos, como un personal motivado y altamente productivo , acordes a las exigencia del sector agrícola y agroindustrial.<br>*Incorporar tecnología altamente alternativas de carácter renovable, compatible con el medio ambiente.<br>

      </p>
  <br>

  </article>
  </section>
@endsection
