<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', function () {
    return view('inicio.index');
});

Route::get('mision', function () {
    return view('mision.index');
});

Route::get('vision', function () {
    return view('vision.index');
});

Route::get('objetivos', function () {
    return view('objetivos.index');
});

Route::get('servicios', function () {
    return view('servicios.index');
});

Route::get('contacto', function () {
    return view('contacto.index');
});

Route::get('verificar-cedula', function () {
    return view('registro.verificar_cedula');
});

Route::post('validar-cedula', 'EmpleadoController@verificarCedula')->name('registro.validarCedula');

Route::get('registro', function () {
    return redirect('verificar-cedula');
});


Route::get('registro/{cedula}', 'EmpleadoController@registro')->name('registro.create');

Route::post('guardar', 'EmpleadoController@guardar')->name('registro.guardar');

Route::get('login', 'Auth\LoginController@getLogin')->name('login');

Route::post('login', 'Auth\LoginController@login')->name('authenticate');

Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function () {

  Route::get('panel', function () {
      return view('panel.index');
  });

  Route::group(['middleware' => 'admin'], function () {

    Route::get('empleados', 'EmpleadoController@listado')->name('listado');

    Route::get('empleado/{id}/editar', 'EmpleadoController@editarEmpleado')->name('empleado.editar');

    Route::get('empleado/agregar-cedula', 'EmpleadoController@createCedula')->name('empleado.createCedula');

    Route::post('empleado/agregar/cedula', 'EmpleadoController@agregarCedula')->name('empleado.agregarCedula');

    Route::post('empleado/actualizar', 'EmpleadoController@actualizarEmpleado')->name('empleado.actualizar');

    Route::get('empleado/{id}/contrato', 'EmpleadoController@contrato')->name('contrato');

    Route::get('empleado/{id}/trabajo', 'EmpleadoController@trabajo')->name('trabajo');

    Route::post('empleado/contrato/actualizar', 'EmpleadoController@actualizarContrato')->name('contrato.actualizar');

    Route::post('empleado/nomina/agregar', 'EmpleadoController@agregarNomina')->name('trabajo.agregar');

  });

  Route::get('perfil', 'EmpleadoController@perfil')->name('perfil');

  Route::post('perfil/actualizar', 'EmpleadoController@actualizarPerfil')->name('perfil.actulizar');

  Route::get('recibos-pagos', 'EmpleadoController@recibos')->name('recibo');

  Route::get('recibos-pagos/{id}/ver', 'EmpleadoController@verRecibo')->name('recibo.ver');

  Route::get('recibos-pagos/{id}/imprimir', 'EmpleadoController@impimirRecibo')->name('recibo.imprimir');

  Route::get('constancia', 'EmpleadoController@constancia')->name('constancia');

  Route::get('pdfconstancia', 'EmpleadoController@pdfConstancia')->name('constancia.pdf');

});
