<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
     protected $redirectTo = '/panel';
     protected $redirectPath = '/panel';
     protected $loginPath = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function getLogin(){
      return view('login.index');
    }

    public function login(Request $request)
      {
          $username =$request->username;
          $password =$request->password;

          //dd($request->all());
          if (Auth::attempt(['username' => $username, 'password' => $password])) {
              // Authentication passed...
              return redirect()->intended('panel');
          }

          return redirect('login')->with('status_error', 'El usuario y la contraseña no son valido intente nuevamente');
      }

}
