<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Empleado;
use App\Nomina;
use App\CedulaTrabajador;
use Carbon\Carbon;
use Jenssegers\Date\Date;
use PDF;

class EmpleadoController extends Controller
{
    public function verificarCedula(Request $request){
      $verificarCedula = CedulaTrabajador::where('cedula',$request->cedula)->get();
      if($verificarCedula->isEmpty()){
        return redirect('verificar-cedula')->with('status_error', 'Usted no pertenece al personal de planimara, por lo tanto no se puede llevar a cabo su registro.');
      }else{
        $cedula=$request->cedula;
        $verificacion=$verificarCedula->first();
        if($verificacion->estatus==1){
          return redirect('verificar-cedula')->with('status_error', 'La cedula suminitrada ya se encuentra agregada');
        }
        return redirect()->route('registro.create',['cedula'=>$cedula]);
      }
    }

    public function registro(Request $request, $cedula){
      if(isset($cedula)){
        return view('registro.create',compact('cedula'));
      }
        return redirect('verificar-cedula')->with('status_error', 'Introduzca su cedula por favor');
    }

    public function guardar(Request $request){

      $this->validate($request, [
        'nombre' => 'required',
        'apellido' => 'required',
        'cedula' => 'required|unique:usuarios,cedula|digits_between:7,8',
        'usuario' => 'required|unique:usuarios,username|alpha_dash',
        'password' => 'required',
      ]);
      //dd($request);
      $verificarCedula = CedulaTrabajador::where('cedula',$request->cedula)->get();
      if($verificarCedula->count() < 1){
        return redirect('verificar-cedula')->with('status_error', 'Usted no pertenece al personal de planimara, por lo tanto no se puede llevar a cabo su registro.');
      }else{
        $verificacion=$verificarCedula->first();
        if($verificacion->estatus==1){
          return redirect('verificar-cedula')->with('status_error', 'La cedula suminitrada ya se encuentra agregada');
        }
      }

      $usuario = new Usuario();
      $usuario->nombre = $request->nombre;
      $usuario->apellido = $request->apellido;
      $usuario->cedula = $request->cedula;
      $usuario->username = $request->usuario;
      $usuario->password = bcrypt($request->password);
      $usuario->tipo = 'empleado';
      $usuario->save();

      $empleado = new Empleado();
      $empleado->usuario_id=$usuario->id;
      $empleado->cargo = '';
      $empleado->oficina = '';
      $empleado->monto = 0;
      $empleado->fecha_ingreso = '01/01/2017';
      $empleado->save();

      $verificacion->estatus=1;
      $verificacion->save();

      return redirect()->route('login')->with('status_success', 'Se ha realizado el registro correctamente ya puede iniciar sesión');
      //return redirect()->back()->withInput();;
    }

    public function listado(){
      $empleados = Usuario::where('tipo','empleado')->get();
      return view('empleado.index',compact('empleados'));
    }

    public function contrato($id){
      $empleado = Usuario::where('tipo','empleado')->findOrFail($id);
      $contrato = Empleado::where('usuario_id',$id)->first();
      return view('empleado.contrato',compact('empleado','contrato'));
    }

    public function createCedula(){
      return view('usuario.agregar-cedula');
    }

    public function agregarCedula(Request $request){
      $this->validate($request, [
        'cedula' => 'required|unique:cedula_trabajadores,cedula|digits_between:7,8',
      ]);

      $guardar = new CedulaTrabajador();
      $guardar->cedula=$request->cedula;
      $guardar->estatus='0';
      $guardar->save();

      return redirect()->route('empleado.createCedula')->with('status_success', 'Se ha agregado correctamente');
    }

    public function perfil(){
      $id=auth()->user()->id;
      $perfil = Usuario::findOrFail($id);
      return view('usuario.perfil',compact('perfil'));
    }


    public function actualizarPerfil(Request $request){

          $id=auth()->user()->id;

          $this->validate($request, [
            'usuario' => 'required|unique:usuarios,username,'.$id.',id|alpha_dash',
          ]);

          $usuario = Usuario::findOrFail($id);
          $usuario->username=$request->usuario;
          if(!empty($request->password)){
            $usuario->password = bcrypt($request->password);
          }
          $usuario->save();

          return redirect()->route('perfil')->with('status_success', 'Se ha realizado la modificación correctamente');
    }



    public function editarEmpleado($id){
      $empleado = Usuario::where('tipo','empleado')->findOrFail($id);
      return view('usuario.editar',compact('empleado'));
    }


    public function actualizarEmpleado(Request $request){

          $id=$request->id;

          $this->validate($request, [
            'nombre' => 'required',
            'apellido' => 'required',
            'cedula' => 'required|unique:usuarios,cedula,'.$id.',id|digits_between:7,8',
            'usuario' => 'required|unique:usuarios,username,'.$id.',id|alpha_dash'

          ]);

          $usuario = Usuario::where('tipo','empleado')->findOrFail($id);
          $usuario->nombre = $request->nombre;
          $usuario->apellido = $request->apellido;
          $usuario->cedula = $request->cedula;
          $usuario->username = $request->usuario;
          if(!empty($request->password)){
            $usuario->password = bcrypt($request->password);
          }
          $usuario->save();

          return redirect()->route('empleado.editar',['id'=>$request->id])->with('status_success', 'Se ha realizado la modificación correctamente');
    }


    public function actualizarContrato(Request $request){

      $this->validate($request, [
        'cargo' => 'required',
        'oficina' => 'required',
        'sueldo' => 'required|numeric',
        'fecha_ingreso' => 'required',
      ]);

      $cargo=$request->cargo;
      $oficina=$request->oficina;
      $sueldo=$request->sueldo;
      $id=$request->id;
      $usuario_id=$request->usuario_id;
      $fecha_ingreso=$request->fecha_ingreso;

      //dd($fech)

      $contrato = Empleado::where('usuario_id',$id)->findOrFail($id);
      $contrato->cargo=$cargo;
      $contrato->oficina=$oficina;
      $contrato->monto=$sueldo;
      $contrato->fecha_ingreso=$request->fecha_ingreso;
      $contrato->save();

      return redirect()->route('contrato',['id'=>$usuario_id])->with('status_success', 'Se ha modificado satifactoriamente');
    }

    public function trabajo($id){
      $empleado = Usuario::where('tipo','empleado')->findOrFail($id);
      $contrato = Empleado::where('usuario_id',$id)->first();

      return view('empleado.trabajo',compact('empleado','contrato'));
    }

    public function agregarNomina(Request $request){

      $this->validate($request, [
        'fecha_desde' => 'required',
        'fecha_hasta' => 'required',
        'dias_trabajados' => 'required|integer',
        'dias_de_descanso' => 'required|integer',
        'sabados_trabajados' => 'integer',
        'domingos_trabajados' => 'integer',
        'horas_extras' => 'integer',
        'otros_ingresos' => 'numeric',
        'prestamos_o_adelantos' => 'numeric',
        'otras_deducciones' => 'numeric',
      ]);

      $fecha_desde=$request->fecha_desde;
      $fecha_hasta=$request->fecha_hasta;
      $dias_trabajados=$request->dias_trabajados;
      $dias_de_descanso=$request->dias_de_descanso;
      $sabados_trabajados=empty($request->sabados_trabajados) ? 0 : $request->sabados_trabajados;
      $domingos_trabajados=empty($request->domingos_trabajados) ? 0 : $request->domingos_trabajados;
      $horas_extras=empty($request->horas_extras) ? 0 : $request->horas_extras;
      $otros_ingresos=empty($request->otros_ingresos) ? 0 : $request->otros_ingresos;
      $prestamos_o_adelantos=empty($request->prestamos_o_adelantos) ? 0 : $request->prestamos_o_adelantos;
      $otras_deducciones=empty($request->otras_deducciones) ? 0 : $request->otras_deducciones;
      $usuario_id=$request->usuario_id;


      $contrato = Empleado::where('usuario_id',$usuario_id)->first();
      if($contrato->monto < 1){
        return redirect()->route('trabajo',['id'=>$usuario_id])->with('status_warning', 'La solicitud no puede ser procesada el sueldo debe ser mayor a cero');
      }
      $sueldo = $contrato->monto;

      $nomina = new Nomina();
      $nomina->fecha_desde=$fecha_desde;
      $nomina->fecha_hasta=$fecha_hasta;
      $nomina->dias_trabajados=$dias_trabajados;
      $nomina->dias_de_descanso=$dias_de_descanso;
      $nomina->sabados_trabajados=$sabados_trabajados;
      $nomina->domingos_trabajados=$domingos_trabajados;
      $nomina->horas_extras=$horas_extras;
      $nomina->otros_ingresos=$otros_ingresos;
      $nomina->prestamos_o_adelantos=$prestamos_o_adelantos;
      $nomina->otras_deducciones=$otras_deducciones;
      $nomina->sueldo=$sueldo;
      $nomina->usuario_id=$usuario_id;
      $nomina->save();

      return redirect()->route('trabajo',['id'=>$usuario_id])->with('status_success', 'Se ha procesado satifactoriamente');

    }

    public function recibos(){

      $usuario_id = auth()->user()->id;
      $empleado = Usuario::where('tipo','empleado')->findOrFail($usuario_id);
      $contrato = Empleado::where('usuario_id',$usuario_id)->first();
      $recibos= Nomina::where('usuario_id',$usuario_id)->get();

      return view('recibo.index',compact('empleado','contrato','recibos'));
    }

    public function verRecibo($id){

      $recibo= Nomina::findOrFail($id);

      $usuario_id = $recibo->usuario_id;

      $empleado = Usuario::where('tipo','empleado')->findOrFail($usuario_id);
      $contrato = Empleado::where('usuario_id',$usuario_id)->first();

      $sueldo_diario = $recibo->sueldo/30;
      $sueldo_por_hora = ($recibo->sueldo/30)/8;
      $sueldo_por_hora_fines_semana =($sueldo_diario*1.5);
      $sueldo_horas_extras=($sueldo_por_hora*1.5);
      $total_dias_trabajadas = $sueldo_diario * $recibo->dias_trabajados;
      $total_dias_de_descanso = $sueldo_diario * $recibo->dias_de_descanso;
      $total_sabados_trabajados = ($sueldo_diario*1.5) * $recibo->sabados_trabajados;
      $total_domingos_trabajados = ($sueldo_diario*1.5) * $recibo->domingos_trabajados;
      $total_horas_extras = ($sueldo_por_hora*1.5) * $recibo->horas_extras;
      $total_otros_ingresos = $recibo->otros_ingresos;

      $total_prestamo_adelanto = $recibo->prestamos_o_adelantos;
      $total_otras_deducciones = $recibo->otras_deducciones;
      $total_retencion_lph = ($sueldo_diario*0.01)*($recibo->dias_trabajados+$recibo->dias_de_descanso);

      //dd($recibo->fecha_desde);
      $first=Carbon::parse(Carbon::createFromFormat('d/m/Y', $recibo->fecha_desde)->format('Y/m/d'));
      $second=Carbon::parse(Carbon::createFromFormat('d/m/Y', $recibo->fecha_hasta)->format('Y/m/d'));
      //$fecha_ini=Carbon::createFromFormat('d/m/Y', $recibo->fecha_desde)->format('Y/m/d');
      //$dt = Carbon::create(2014, 1, 1);
      //$dt2 = Carbon::create(2014, 12, 31);
      $daysForExtraCoding = $first->diffInDaysFiltered(function(Carbon $date) {
         return $date->dayOfWeek == Carbon::MONDAY;
      }, $second);

      $total_retencion_sos = ((($recibo->sueldo*0.04)/52)*12)*$daysForExtraCoding;
      $total_retencion_pf = ((($recibo->sueldo*(0.005))/52)*12)*$daysForExtraCoding;


      $total_acumulado=$total_dias_trabajadas+$total_dias_de_descanso+$total_sabados_trabajados+$total_domingos_trabajados+$total_horas_extras+$total_otros_ingresos;
      $total_retenciones=$total_prestamo_adelanto+$total_otras_deducciones+$total_retencion_lph+$total_retencion_sos+$total_retencion_pf;
      $total=$total_acumulado-$total_retenciones;

      $resultado=[
        'sueldo_diario'=>$sueldo_diario,
        'sueldo_por_hora'=>$sueldo_por_hora,
        'sueldo_por_hora_fines_semana'=>$sueldo_por_hora_fines_semana,
        'sueldo_horas_extras'=>$sueldo_horas_extras,
        'total_dias_trabajadas'=>$total_dias_trabajadas,
        'total_dias_de_descanso'=>$total_dias_de_descanso,
        'total_sabados_trabajados'=>$total_sabados_trabajados,
        'total_domingos_trabajados'=>$total_domingos_trabajados,
        'total_horas_extras'=>$total_horas_extras,
        'total_otros_ingresos'=>$total_otros_ingresos,
        'total_prestamo_adelanto'=>$total_prestamo_adelanto,
        'total_otras_deducciones'=>$total_otras_deducciones,
        'total_retencion_lph'=>$total_retencion_lph,
        'total_retencion_sos'=>$total_retencion_sos,
        'total_retencion_pf'=>$total_retencion_pf,
        'total_acumulado'=>$total_acumulado,
        'total_retenciones'=>$total_retenciones,
        'total'=>$total,
      ];

      //dd($resultado);


      return view('recibo.ver',compact('empleado','contrato','recibo','resultado'));
    }

    public function constancia(){

      $usuario_id = auth()->user()->id;
      $empleado = Usuario::where('tipo','empleado')->findOrFail($usuario_id);
      $contrato = Empleado::where('usuario_id',$usuario_id)->first();

      $date = $date = new Date(Carbon::now());

      return view('constancia.index',compact('empleado','contrato','date'));
    }

    public function pdfConstancia(){

      $usuario_id = auth()->user()->id;
      $empleado = Usuario::where('tipo','empleado')->findOrFail($usuario_id);
      $contrato = Empleado::where('usuario_id',$usuario_id)->first();

      $date = $date = new Date(Carbon::now());

      $img='principal.png';
      $path=public_path() . '/imagenes/principal.png';
      $type = pathinfo($path, PATHINFO_EXTENSION);
      $im = file_get_contents($path);
      $imdata = base64_encode($im);

      $pdf = PDF::loadView('constancia.rpt_constancia', [
             'empleado' => $empleado,
             'contrato'=>$contrato,
             'date'=>$date,
             'logo'=>$imdata,
             'type_image'=>$type,
      ]);

      $nombre=str_slug('constancia_'.$empleado->nombre.'-'.$empleado->apellido.'-'.$empleado->cedula);
      return $pdf->stream($nombre.'.pdf');
    }

    public function impimirRecibo($id){

      $recibo= Nomina::findOrFail($id);

      $usuario_id = $recibo->usuario_id;

      $empleado = Usuario::where('tipo','empleado')->findOrFail($usuario_id);
      $contrato = Empleado::where('usuario_id',$usuario_id)->first();

      $sueldo_diario = $recibo->sueldo/30;
      $sueldo_por_hora = ($recibo->sueldo/30)/8;
      $sueldo_por_hora_fines_semana =($sueldo_diario*1.5);
      $sueldo_horas_extras=($sueldo_por_hora*1.5);
      $total_dias_trabajadas = $sueldo_diario * $recibo->dias_trabajados;
      $total_dias_de_descanso = $sueldo_diario * $recibo->dias_de_descanso;
      $total_sabados_trabajados = ($sueldo_diario*1.5) * $recibo->sabados_trabajados;
      $total_domingos_trabajados = ($sueldo_diario*1.5) * $recibo->domingos_trabajados;
      $total_horas_extras = ($sueldo_por_hora*1.5) * $recibo->horas_extras;
      $total_otros_ingresos = $recibo->otros_ingresos;

      $total_prestamo_adelanto = $recibo->prestamos_o_adelantos;
      $total_otras_deducciones = $recibo->otras_deducciones;
      $total_retencion_lph = ($sueldo_diario*0.01)*($recibo->dias_trabajados+$recibo->dias_de_descanso);

      //dd($recibo->fecha_desde);
      $first=Carbon::parse(Carbon::createFromFormat('d/m/Y', $recibo->fecha_desde)->format('Y/m/d'));
      $second=Carbon::parse(Carbon::createFromFormat('d/m/Y', $recibo->fecha_hasta)->format('Y/m/d'));
      //$fecha_ini=Carbon::createFromFormat('d/m/Y', $recibo->fecha_desde)->format('Y/m/d');
      //$dt = Carbon::create(2014, 1, 1);
      //$dt2 = Carbon::create(2014, 12, 31);
      $daysForExtraCoding = $first->diffInDaysFiltered(function(Carbon $date) {
         return $date->dayOfWeek == Carbon::MONDAY;
      }, $second);

      $total_retencion_sos = ((($recibo->sueldo*0.04)/52)*12)*$daysForExtraCoding;
      $total_retencion_pf = ((($recibo->sueldo*(0.005))/52)*12)*$daysForExtraCoding;


      $total_acumulado=$total_dias_trabajadas+$total_dias_de_descanso+$total_sabados_trabajados+$total_domingos_trabajados+$total_horas_extras+$total_otros_ingresos;
      $total_retenciones=$total_prestamo_adelanto+$total_otras_deducciones+$total_retencion_lph+$total_retencion_sos+$total_retencion_pf;
      $total=$total_acumulado-$total_retenciones;

      $resultado=[
        'sueldo_diario'=>$sueldo_diario,
        'sueldo_por_hora'=>$sueldo_por_hora,
        'sueldo_por_hora_fines_semana'=>$sueldo_por_hora_fines_semana,
        'sueldo_horas_extras'=>$sueldo_horas_extras,
        'total_dias_trabajadas'=>$total_dias_trabajadas,
        'total_dias_de_descanso'=>$total_dias_de_descanso,
        'total_sabados_trabajados'=>$total_sabados_trabajados,
        'total_domingos_trabajados'=>$total_domingos_trabajados,
        'total_horas_extras'=>$total_horas_extras,
        'total_otros_ingresos'=>$total_otros_ingresos,
        'total_prestamo_adelanto'=>$total_prestamo_adelanto,
        'total_otras_deducciones'=>$total_otras_deducciones,
        'total_retencion_lph'=>$total_retencion_lph,
        'total_retencion_sos'=>$total_retencion_sos,
        'total_retencion_pf'=>$total_retencion_pf,
        'total_acumulado'=>$total_acumulado,
        'total_retenciones'=>$total_retenciones,
        'total'=>$total,
      ];

      //dd($resultado);
      $img='principal.png';
      $path=public_path() . '/imagenes/principal.png';
      $type = pathinfo($path, PATHINFO_EXTENSION);
      $im = file_get_contents($path);
      $imdata = base64_encode($im);

      $pdf = PDF::loadView('recibo.rpt_recibo', [
             'empleado' => $empleado,
             'contrato'=>$contrato,
             'recibo'=>$recibo,
             'resultado'=>$resultado,
             'logo'=>$imdata,
             'type_image'=>$type,
      ]);

      $nombre=str_slug('recibo_'.$empleado->nombre.'-'.$empleado->apellido.'-'.$empleado->cedula);
      return $pdf->stream($nombre.'.pdf');


      //return view('recibo.ver',compact('empleado','contrato','recibo','resultado'));
    }

}
