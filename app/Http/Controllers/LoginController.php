<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class LoginController extends Controller
{
  /**
   * Where to redirect users after login.
   *
   * @var string
   */
  protected $redirectTo = '/panel';
  protected $redirectPath = '/panel';
  protected $loginPath = '/login';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('guest', ['except' => 'logout']);
  }

  public function login(){
    return view('login.index');
  }

  public function authenticate(Request $request)
    {
        $username =$request->username;
        $password =bcrypt($request->password);
        if (Auth::attempt(['username' => $username, 'password' => $password])) {
            // Authentication passed...
            return redirect()->intended('panel');
        }

        dd(Auth::attempt(['username' => $username, 'password' => $password]));
    }


}
