<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Nomina extends Model
{
  protected $table='nominas';
  protected $date = ['fecha_desde','fecha_hasta'];

  public function setFechaDesdeAttribute($value)
  {
       $this->attributes['fecha_desde'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
  }

  public function getFechaDesdeAttribute($value)
  {
       return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
  }

  public function setFechaHastaAttribute($value)
  {
       $this->attributes['fecha_hasta'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
  }

  public function getFechaHastaAttribute($value)
  {
       return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
  }
}
