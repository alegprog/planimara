<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Empleado extends Model
{
    protected $table='empleados';
    protected $date = ['fecha_ingreso'];

   public function setFechaIngresoAttribute($value)
   {
        $this->attributes['fecha_ingreso'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
   }

   public function getFechaIngresoAttribute($value)
   {
        return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y');
   }
}
