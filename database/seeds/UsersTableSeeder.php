<?php

use Illuminate\Database\Seeder;
use App\Usuario;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Usuario();
        $admin->nombre='Administrador';
        $admin->apellido='de Sistema';
        $admin->cedula='00000000';
        $admin->username='administrador';
        $admin->password=bcrypt('admin');
        $admin->tipo='administrador';
        $admin->save();
    }
}
