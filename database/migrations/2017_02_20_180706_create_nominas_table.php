<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNominasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nominas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_desde');
            $table->date('fecha_hasta');
            $table->integer('dias_trabajados');
            $table->integer('dias_de_descanso');
            $table->integer('sabados_trabajados');
            $table->integer('domingos_trabajados');
            $table->integer('horas_extras');
            $table->double('otros_ingresos',15,2);
            $table->double('prestamos_o_adelantos',15,2);
            $table->double('otras_deducciones',15,2);
            $table->double('sueldo',15,2);
            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nominas');
    }
}
